package com.gitlab.tclavier;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PolynomialTest {
    @Test
    void should_render_digit() {
        assertEquals("0", Polynomial.of(0).pretty());
        assertEquals("1", prettify(1));
    }

    @Test
    void should_render_indeterminate() {
        assertEquals("-3x + 2", prettify(2, -3));
    }

    @Test
    void should_replace_addition_and_unary_minus_by_minus() {
        assertEquals("3x - 2", prettify(-2, 3));
    }


    @Test
    void should_remove_null_monomer() {
        assertEquals("5x", prettify(0, 5));
    }

    @Test
    void should_remove_factor_one() {
        assertEquals("x", prettify(0, 1));
    }


    @Test
    void should_replace_factor_minus_one_by_minus() {
        assertEquals("-x", prettify(0, -1));
    }


    @Test
    void should_show_power_symbol() {
        assertEquals("-3x^2 + 2x + 6", prettify(6, 2, -3));
    }


    @Test
    void should_remove_null_monomer_indeterminate() {
        assertEquals("-3x^2 + 6", prettify(6, 0, -3));
    }

    @Test
    void full_test() {
        assertEquals("x^5 - 9x^3 - 2x + 1", prettify(1, -2, 0, -9, 0, 1));
    }

    private String prettify(int... monomers) {
        return Polynomial.of(monomers).pretty();
    }

}
