package com.gitlab.tclavier;

import java.util.ArrayList;
import java.util.List;

public class Polynomial {
    private int[] monomials;

    public static Polynomial of(int... monomers) {
        Polynomial polynomial = new Polynomial();
        polynomial.monomials = monomers;
        return polynomial;
    }

    public String pretty() {
        List<String> acc = new ArrayList<>();
        for (int degree = monomials.length - 1; degree >= 0; degree--) {
            int factor = monomials[degree];
            addNonEmpty(acc, getSign(acc.size(), factor));
            addNonEmpty(acc, getFactor(factor, acc.size(), degree) + getIndeterminate(degree, factor));
        }
        return String.join(" ", acc);
    }

    private String getIndeterminate(int degree, int factor) {
        if (factor == 0) return "";
        if (degree == 1) return "x";
        if (degree > 1) return "x^" + degree;
        return "";
    }

    private String getFactor(int factor, int size, int degree) {
        if (factor == 1 && degree > 0) return "";
        if (factor == -1 && degree > 0) return "-";
        if (factor == 0 && size > 0) return "";
        if (factor < 0 && size > 0) return String.valueOf(-factor);
        return String.valueOf(factor);
    }

    private void addNonEmpty(List<String> acc, String token) {
        if (!"".equals(token))
            acc.add(token);
    }

    private String getSign(int size, int factor) {
        if (factor == 0)
            return "";
        if (size == 0)
            return "";
        if (factor < 0)
            return "-";
        return "+";
    }
}
