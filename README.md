Il s'agit d'afficher le plus joliment possible un polynome à coefficients entiers (positifs).

Le polynôme est  représenté par un tableau d'entiers.

1 2 3 => 3x^2 + 2x + 1

les règles d'usages seront les suivantes:

* les monômes sont affichées du degré fort vers les degrés faibles
* un monôme à coefficient nul n'est affiché que pour le polynome nul = 0
* un monôme avec un coefficient de 1 n'affiche ce coefficient que pour le degré 0
* les exposants donnent l'affichage ( rien, x ou x^n selon que l'exposant est nul, égal à 1 ou autre)
* les opérateurs + et - séparent les monômes (les coefficients négatifs n'apparaissent négatifs que si ils sont en tête de la représentation)

